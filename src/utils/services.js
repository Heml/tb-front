import axios from 'axios'


function getAll() {
  let request = axios.get('/iecho/words').then(res => res.data
  ).catch(err => {
    return { ...err, error: 'true' }
  })
  return request
}

function setNew(word) {
  if (word) {
    axios.get('/iecho', {
      params: {
        text: word
      }
    }).then(res => {
      return {
        status: 'ok',
        code: res.status // stands for ok
      }
    }).catch(err => {
      return {
        status: 'bad request',
        ...err
      }
    })
  } else {
    return {
      status: 'bad request',
      code: 400 // stands for bad request
    }
  }
}

export {
  getAll,
  setNew
}