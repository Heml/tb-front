const store = require('store2')

class wordsHandler {
  #data;
  constructor() {
    this.#data = store.get('words')
  }

  add(word) {
    store.set('words', [...this.#data || [], word])
  }

  get() {
    return this.#data
  }
}

module.exports = wordsHandler
