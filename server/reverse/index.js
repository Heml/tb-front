const express = require('express')
const morgan = require('morgan')
const reverse = require('./services/reverse')
const wordsHandler = require('./services/wordsHandler')

var router = express.Router()
router.use(morgan('dev'))

router.get('/', (req, res) => {
  const db = new wordsHandler()
  const request = reverse(req.query.text)
  if(request.err){
    res.status(400)
    res.json({
      error: 'no text'
    })
  }else{
    db.add(request)
  }

  res.status(200).json({...request})
})


router.get('/words', (req, res) => {
  const db = new wordsHandler()
  res.json(
    db.get()
  )
})

export default router