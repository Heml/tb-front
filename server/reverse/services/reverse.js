const palindome = require('./palindrome')
/**
 * Returns a reversed string
 * @param {*} string 
 * @returns String
 */
function reverse(string){
  if(!string){
    return {
      err: true
    }
  }


  let stringArr = string.split('')
  stringArr = stringArr.reverse()
  const newString = stringArr.join('')
  return {
    text: newString,
    palindrome: palindome(newString, string)
  }
}

module.exports = reverse