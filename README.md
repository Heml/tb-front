## How it works
- Go to the app and type whatever you want to reverse and press enter, you can also check if it's a palindrome!

**As a plus** that I wanted to add, the App stores all of the requests send by everyone to the server, so you can see all of the words that people all over the world have entered!
## How to run the project
- ``npm install``
- ``npm run dev`` 

There are no enviroment variables for this project, you can run it as easy as running the two previous commands.

## How it was build

it was built using a monolithic repo architecture using server side rendering.

Technologies used:

- React
- Node
- store2 (server storage management for storing the data)
- webpack
- styled-components
- react-bootstrap


## Testing the api
supertest, mocha and chai were used, just run ``npm test``.


## Git flow

- Handling to branches, one ``dev`` and ``main`` in which we first push the changes to ``dev`` and then marge it with main.