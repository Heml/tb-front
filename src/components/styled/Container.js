import styled from 'styled-components';

const Container = styled.div`
${({ textAlign = 'center', width = '70%' }) => `
  margin: auto;
  width: ${width};
  text-align: ${textAlign};
`}  
`

export default Container;