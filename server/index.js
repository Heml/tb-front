import express from 'express'
import path from 'path'
import React from 'react'
import ReactDOMServer from 'react-dom/server'
import { ServerStyleSheet } from 'styled-components';
import Html from './htmlTemplate'
import App from '../src/App'
import reverse from './reverse'

const PORT = 8000;

const app = express()

app.use('/iecho', reverse)

app.use('^/$', (req, res, next) => {
  try {
    const sheet = new ServerStyleSheet();
    const body = ReactDOMServer.renderToString(sheet.collectStyles(<App />)); 
    const styles = sheet.getStyleTags(); 
    const title = 'Reversing words!';


    res.send(
      Html({
        body,
        styles,
        title
      })
    )
  } catch (err) {
    res.json(err)
  }
})

app.use(express.static('public'))

app.listen(PORT, () => console.log('listening on port ' + PORT))