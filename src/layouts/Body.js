import styled from 'styled-components'
import React from 'react'
import BodyTemplate from '../components/styled/Body'
import CustomContainer from '../components/styled/Container'

const Title = styled.h1`
  font-size: 50px;
  margin-left: 30px;
  padding: 20px;
`

const Span = styled.span`
  ${({color = '#000'}) => `
    color: ${color};
  `}
`

const Ul = styled.ul`
  list-style: none;
  li{
    padding: 20px;
    width: 100%;
    background: white;
    font-size: 18px;
  }
`
export default function Body({ elements = [] }) {
  return (
    <BodyTemplate>
      <Title>Results</Title>
      <CustomContainer>
        <CustomContainer width='70%' textAlign='left'>
        <Ul>
          {elements && elements.map(({ text, palindrome }, idx) => {
            return (
              <li key={`li-${idx}-${text}`}>
                <span>{text}</span>
                <Span color={palindrome ? 'green' : 'red'}>
                  {palindrome ? ' Palindrome' : ' Not Palindome'}
                </Span>
              </li>
            )
          })}
        </Ul>
        </CustomContainer>
      </CustomContainer>
    </BodyTemplate>
  )
}