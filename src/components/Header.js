import React, { useState } from 'react'
import { Navbar, Container, Form, FormControl, Button } from 'react-bootstrap'
import CustomContainer from '../components/styled/Container'
import { setNew, getAll } from '../utils/services'

export default function Header({setElements}) {
  const [word, setWord] = useState('')
  function newWord(e) {
    setWord(e.target.value)
  }

  function setNewWord(e) {
    e.preventDefault()
    setNew(word)
    getAll().then(data => setElements(data)).catch(err => setElements([]))
  }
  return (
    <Navbar bg="danger" expand="lg">
      <Container fluid className='justify-content-center'>
        <Navbar.Collapse id="navbarScroll justify-content-center">
          <CustomContainer>
            <Form onSubmit={setNewWord} className="d-flex">
              <FormControl
                onChange={newWord}
                type="search"
                placeholder="Search"
                className="me-2"
                aria-label="Search"
              />
              <Button variant="success">Search</Button>
            </Form>
          </CustomContainer>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}