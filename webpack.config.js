var nodeExternals = require('webpack-node-externals')
const path = require('path');

const server = {
  watch: true,
  mode: 'development',
  target: 'node',
  externals: [nodeExternals()],
  entry: path.join(__dirname, "server"),
  output: {
    path: path.resolve(__dirname, "server-build"),
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'isomorphic-style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          },
          'postcss-loader'
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-url-loader',
            options: {
              limit: 10000,
              jsx: true // true outputs JSX tags
            },
          },
        ],
      },
      {
        test: /\.?js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
      },
    ]
  },
}

module.exports = server