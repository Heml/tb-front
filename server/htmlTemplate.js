export default ({ body, styles, title }) => `
<!DOCTYPE html>
<html>
  <head>
    <title>${title}</title>
    ${styles}
    <link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
  integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
  crossorigin="anonymous"
/>
  </head>
  <body style="margin:0">
    <div id="root">${body}</div>

    <script src='./bundle.js'></script>
  </body>
</html>
`;