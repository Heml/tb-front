const { expect } = require('chai')
const chai = require('chai')
const request = require('supertest')
const server = 'http://localhost:8000'
const reverse = require('../services/reverse')

chai.should()

describe('reverse api', () => {
  const PARAMS = {
    TEXT: 'some',
    REVERSED_TEXT: function () {
      return reverse(this.TEXT).text
    }
  }

  describe('iecho endpoint', () => {
    it('should return the text passed backwords', (done) => {
      request(server)
        .get("/iecho").query({ text: PARAMS.TEXT })
        .expect(200)
        .expect('Content-Type', /json/)
        .then((res) => {
          expect(res.body).to.have.property('text').to.be.equal(PARAMS.REVERSED_TEXT())
          done()
        })
    })
  })
})

