import React, { useEffect, useState } from 'react'
import Header from './components/Header'
import Body from './layouts/Body';
import {getAll} from './utils/services'

function App() {
  const [elements, setElements] = useState([])

  useEffect(() => {
    const getElements = getAll()
    getElements.then(data => setElements(data))
  }, [])
  return (
    <div className="App">
      <Header setElements={setElements}/>
      <Body elements={ elements } />
    </div>
  );
}

export default App;
