import React from "react";
import styled from "styled-components";
import { Row } from "react-bootstrap";

export default styled(Row)`
  ${({background = '#fafafa'}) => `
    background: ${background};
  `}
`